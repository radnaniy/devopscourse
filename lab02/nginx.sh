#! /bin/bash
apt-get -y full-upgrade
apt-get -y install nginx
curl http://nginx.org/keys/nginx_signing.key | apt-key add -
echo "deb-src http://nginx.org/packages/mainline/ubuntu/ `lsb_release -cs` nginx"  >> /etc/apt/sources.list
echo "deb http://nginx.org/packages/mainline/ubuntu/ `lsb_release -cs` nginx" >> /etc/apt/sources.list
apt-get -y update
apt-get -y install nginx