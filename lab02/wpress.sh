#! /bin/bash
#Updatinfg system packeges 
apt-get -y full-upgrade
#Install PHP & MySQL
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.3 php7.3-fpm  mysql-server php7.3-mysql
#Nginx install
curl http://nginx.org/keys/nginx_signing.key | apt-key add -
echo "deb-src http://nginx.org/packages/mainline/ubuntu/ `lsb_release -cs` nginx"  >> /etc/apt/sources.list
echo "deb http://nginx.org/packages/mainline/ubuntu/ `lsb_release -cs` nginx" >> /etc/apt/sources.list
apt-get -y install nginx
apt-get update
#Configure PHP-FPM
rm /etc/php/7.3/fpm/pool.d/www.conf
cp /vagrant/www.conf /etc/php/7.3/fpm/pool.d/www.conf
#Setup WordPress on Ubuntu
wget http://wordpress.org/latest.tar.gz
tar xzf latest.tar.gz
mkdir -p /var/www/lab2wp.local
mv wordpress /var/www/lab2wp.local
chown -R www-data /var/www/lab2wp.local
chmod -R 755 /var/www/lab2wp.local

# Starting configuring nginx host
#cp /vagrant/lab2.local.conf /etc/nginx/sites-available/lab2.local.conf
# #mkdir -p /var/www/html/lab2wp
# cp /vagrant/wp.conf /etc/nginx/sites-available/nginx.conf
# nginx -t && service nginx restart && echo "Nginx reload seccesfull"
# unlink /etc/nginx/sites-enabled/default
# ln -sf /etc/nginx/sites-available/nginx.conf /etc/nginx/sites-enabled/nginx.conf