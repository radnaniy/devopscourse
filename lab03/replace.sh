PATH=$1
SEARCH=$2
REPLACE=$3
find ${PATH} -type f -exec sed -i "s/${SEARCH}/${REPLACE}/g" {} \;